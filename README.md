**This fork intends to add on GNOME window decorations and colour scheme options to the original theme, of which development had been abandoned since late 2017, and the orignal authors had not responded to my comments that I left back in 2018, leaving me with no choice but to fork it until they can get back to me.**

My changes are being done in this "master" branch, instead of using a separate branch like in the original repository.

- [x] Add GNOME window decorations
- [X] Recreate original gradients in window decorations
- [ ] Recreate original curves in window decorations, to the extent that GNOME 3.x will allow for
- [ ] Recreate different colour schemes that were available in one of the Red Hat Linux or Fedora Core packages for the theme back in the day

I currently have no plans to support the upcoming GTK 4 with this.

Modifications are only being done in the "gtk-3.20" and "gtk-3.24" directories on the `master` branch only, so changes won't affect older GTK versions, as I am quite busy and have other projects to work on and/or contribute to.

It appeared some of the elements in this theme were drawn using PNG files, most likely due to the limitations of GTK 3 using CSS, and it seems I'll have to do the same too, as there doesn't seem to be a way to stack CSS background elements. I've chosen to base the window decorations on the original Red Hat Linux version (known in Fedora as the package `Bluecurve-classic`), rather than the later version used in Fedora Core 1 and 2.

Original README file can be found in [README.original.md](README.original.md).
